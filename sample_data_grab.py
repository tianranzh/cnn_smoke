
# Author: Tianran Zhang
# Contact: tianran.zhang@kcl.ac.uk 
# Date: 2019-04-01 17:00:53
# Last Modified by:  Tianran Zhang 
# Last Modified time: 

                                  #########       
                                 ##       ###     
                        #########      ######     
                    ####             ##           
                  ###                 ##          
               ###              ########          
            ####    #######    ##                 
       #####   #####     ##  ###                  
       ########          ########   


import os
import numpy as np 
import matplotlib.pyplot as plt

dirData = "D:/tf"
dirSave = os.getcwd()
print ("working directory in :", dirSave)

grabData = False  # set False if local data already exist
np.random.seed(42)
imageSize  = 28
imageLayer = 7
NLabels    = 4
labelNames = ['fire','land','cloud','water']

def read_np_data(dataFileName,labelFilename):
    trainData   = np.asarray(np.load(dataFileName)['data'],dtype=np.float32)
    if grabData:
        ### there was a bug in my data generation code, 1/3 axis need to be swap here
        ### this need to be fixed in the future
        trainData = trainData.reshape(-1,imageLayer,imageSize,imageSize)
        trainData = np.swapaxes(trainData,1,3)
        trainData = trainData.reshape(-1,imageLayer*imageSize*imageSize)

    trainLabels = np.asarray(np.load(labelFilename)['label'],dtype=np.int32)
    print ("Reading total training data: ", trainData.shape)
    print ("Reading total training label: ", trainLabels.shape)
    return (trainData,trainLabels)

def cut_sample_data(trainData,trainLabels,cutsize=1000):
    cutData   = trainData[:cutsize,:]
    cutLabels = trainLabels[:cutsize,:]
    return (cutData,cutLabels)

def print_labels_summary(labelsArr):
    argmaxList = np.argmax(labelsArr,axis=1)
    for i in np.arange(NLabels):
        print ('Total num of label', i," (%s) =="%(labelNames[i]), (argmaxList==i).sum())

def plot_random_sample_data(cutData,cutLabels):
    fig,axes = plt.subplots(figsize=(20,15),ncols=imageLayer,nrows=NLabels)
    for i in np.arange(NLabels):
        labelIndList = np.where(cutLabels[:,i]==1)[0]
        pInd = np.random.choice(labelIndList)
        pArr = cutData[pInd,:].reshape(imageSize,imageSize,imageLayer)
        for j in np.arange(imageLayer):
            ax   = axes.flat[i*imageLayer+j]
            ax.imshow(pArr[:,:,j])
            ax.set_axis_off()
            if j==0:
                ax.text(5,5,labelNames[i],fontsize=12,color='white')
    plt.show()
    return

def save_sample_data(cutData,cutLabels):
    np.savez(os.path.join(dirSave,'train_sample.npz'), data=cutData)
    np.savez(os.path.join(dirSave,'labels_sample.npz'), label=cutLabels)
    return

def grab_sample_data_and_cut(dataFileName,labelFilename):
    ### function for T to grab local data and cut a small section as sample
    trainData,trainLabels = read_np_data(dataFileName,labelFilename)
    cutData,cutLabels     = cut_sample_data(trainData,trainLabels)
    return (cutData,cutLabels)

if __name__ == "__main__":
    if grabData:
        dataFileName  = os.path.join(dirData,"tf_train_lwindow.npz")
        labelFilename = os.path.join(dirData,"tf_train_lwindow.npz")
        cutData,cutLabels = grab_sample_data_and_cut(dataFileName,labelFilename)
        save_sample_data(cutData,cutLabels)
    else:
        dataFileName  = os.path.join(dirSave,"train_sample.npz")
        labelFilename = os.path.join(dirSave,"labels_sample.npz")
        cutData,cutLabels = read_np_data(dataFileName,labelFilename)
    
    print_labels_summary(cutLabels)
    plot_random_sample_data(cutData,cutLabels)